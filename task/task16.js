'use strict';

/**
 * Бал
 *
 * По случаю 100500-летия Берляндского государственного университета совсем скоро состоится бал!
 * Уже n юношей и m девушек во всю репетируют вальс, менуэт, полонез и кадриль.
 *
 * Известно, что на бал будут приглашены несколько пар юноша-девушка, причем уровень умений танцевать партнеров
 * в каждой паре должен отличаться не более чем на единицу.
 *
 * Для каждого юноши известен уровень его умения танцевать. Аналогично, для каждой девушки известен уровень
 * ее умения танцевать. Напишите программу, которая определит наибольшее количество пар,
 * которое можно образовать из n юношей и m девушек.
 *
 * Входные данные
 * Первая строка содержит последовательность a1, a2, ..., an (1 ≤ ai ≤ 100), где ai — умение танцевать i-го юноши.
 * Аналогично, вторая строка содержит последовательность b1, b2, ..., bm (1 ≤ bj ≤ 100), где bj — умение танцевать
 * j-й девушки.
 *
 * Выходные данные
 * Выведите единственное число — искомое максимальное возможное количество пар.
 */

var partyTest = [
    {
        parameters: ["1 4 6 2", "5 1 5 7 9"],
        expectedResult: 3
    },
    {
        parameters: ["1 2 3 4", "10 11 12 13"],
        expectedResult: 0
    }
];


function party(boys, girls) {
    //TODO
}


tasks.push({
    title: "Бал",
    solution: party,
    tests: partyTest
});
